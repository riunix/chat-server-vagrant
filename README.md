## Chat Server Riunix Vagrant Box

To Launch Virtual Box using Vagrant

## Requirement
* download Vagrant for Mac https://www.vagrantup.com/downloads.html and install it.

## Installation

``` bash
  $ git clone https://bitbucket.org/riunix/chat-server-vagrant.git
  $ cd chat-server-vagrant
  $ vagrant up
```

## Getting Start

``` bash
  $ telnet 192.168.33.10 8000  
```

